程式的使用說明書
===

程式概述
---
這是用來查詢 Assets 的程式。

程式結構說明
---
以 MVC 架構完成，分別為 Controller Service Model(Assets, AssetsDao) Config.

Config 部分是:

    1. 我寫了 SpringFox 的配置，使用 SWAGGER_2 

Controller 部分是:

    1. 使用 swagger_2 來客製化我想要表達的內容，包含 api 的名稱，以及客製化的 HTTP狀態碼。
    2. 調用 AssetsService 來完成需要判斷的邏輯。
    3. 功能包含 查詢所有物件、查詢特定id物件、新增一筆資料、更新一筆資料、刪除特定id資料

Service 部分是:

    1. api 主要的邏輯。
    2. 使用 AssetsDao 的 Jpa

Assets 部分是:

    1. 和 sqlite 連動，綁定 assets.sqlite 上

AssetsDao 部分是:

    1. 使用 JpaRepository 的配置，方便 Service 調用

注意事項
---
JDK 版本配置

`java version "11"`

SpringBoot 版本配置

`SpringBoot 2.5.6`