package com.martin.mapper;

import com.martin.model.bean.Assets;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AssetsMapper implements RowMapper<Assets> {

    @Override
    public Assets mapRow(ResultSet rs, int rowNum) throws SQLException {
        Assets assets = new Assets();

        assets.setId(rs.getInt("id"));
        assets.setBuiltDate(rs.getString("built_date"));
        assets.setName(rs.getString("name"));
        assets.setUnit(rs.getString("unit"));
        assets.setUser(rs.getString("user"));
        assets.setValue(rs.getDouble("value"));

        return assets;
    }
}
