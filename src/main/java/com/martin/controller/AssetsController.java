package com.martin.controller;

import com.martin.model.bean.Assets;
import com.martin.service.AssetsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@Api(tags = "Assets CRUD")
public class AssetsController {

    @Autowired
    private AssetsService assetsService;

    /**
     * 取得所有資料
     * @return ResponseEntity<List<Assets>>
     */
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 404, message = "沒任何東西")
    })
    @GetMapping("/findAll")
    public ResponseEntity<List<Assets>> findAll() {
        List<Assets> assetsList = assetsService.findAll();

        if (!assetsList.isEmpty()) {
            return new ResponseEntity<>(assetsList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * 取得特定id的資料
     * @param id
     * @return Assets
     */
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "success"),
//            @ApiResponse(code = 404, message = "沒任何東西")
//    })
//    @GetMapping("/findById")
//    public ResponseEntity<Assets> findById(@RequestParam("id") Integer id) {
//        Assets assets = assetsService.findById(id);
//
//        if (assets != null) {
//            return new ResponseEntity<>(assets, HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//    }

    /**
     * 新增一筆資料
     * @param builtDate
     * @param name
     * @param unit
     * @param user
     * @param value
     * @return Assets
     */
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "success"),
//            @ApiResponse(code = 404, message = "新增失敗")
//    })
//    @PostMapping("/create")
//    public ResponseEntity<Assets> create(@RequestParam("builtDate") String builtDate
//            , @RequestParam("name") String name
//            , @RequestParam("unit") String unit
//            , @RequestParam("user") String user
//            , @RequestParam("value") Double value) {
//        Assets assets = new Assets(builtDate, name, unit, user, value);
//
//        Assets saved = assetsService.create(assets);
//
//        if (saved != null) {
//            return new ResponseEntity<>(saved, HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//    }

    /**
     * 更新一筆資料
     * @param id
     * @param builtDate
     * @param name
     * @param unit
     * @param user
     * @param value
     * @return
     */
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "success"),
//            @ApiResponse(code = 404, message = "更新失敗")
//    })
//    @PutMapping("/update")
//    public ResponseEntity<Assets> update(@RequestParam("id") Integer id
//            , @RequestParam("builtDate") String builtDate
//            , @RequestParam("name") String name
//            , @RequestParam("unit") String unit
//            , @RequestParam("user") String user
//            , @RequestParam("value") Double value) {
//        Assets assets = new Assets(builtDate, name, unit, user, value);
//
//        Assets updated = assetsService.update(id, assets);
//
//        if (updated != null) {
//            return new ResponseEntity<>(updated, HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//    }

    /**
     * 刪除特定id的資料
     * @param id
     * @return
     */
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
    })
    @DeleteMapping("/delete")
    public ResponseEntity<Void> delete(@RequestParam("id") Integer id) {
        assetsService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
