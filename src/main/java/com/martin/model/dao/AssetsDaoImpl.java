package com.martin.model.dao;

import com.martin.mapper.AssetsMapper;
import com.martin.model.bean.Assets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AssetsDaoImpl implements AssetsDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate jdbcNameTemplate;

    @Override
    public List<Assets> getAll() {
        String sql = "SELECT * FROM assets";

        return jdbcTemplate.query(sql, new AssetsMapper());
    }

    @Override
    public Assets findByAssetsId(Integer id) {
        String sql = "select * from assets where id = ?";

        Assets assets = jdbcTemplate.queryForObject(sql, new Object[]{id}, new AssetsMapper());

        return assets;
    }

    @Override
    public Assets create(Assets assets) {
        jdbcTemplate.update(
                "insert into assets (built_date, name, unit, user, value) values(?, ?, ?, ?, ?)"
                , assets.getBuiltDate(), assets.getName(), assets.getUnit(), assets.getUser(), assets.getValue());

        return null;
    }

    @Override
    public Assets update(Integer id, Assets assets) {
        return null;
    }

    @Override
    public void delete(Integer id) {
        jdbcTemplate.update("delete from assets where id =?", id);
    }
}
