package com.martin.model.dao;

import com.martin.model.bean.Assets;

import java.util.List;

public interface AssetsDao {
    List<Assets> getAll();

    Assets findByAssetsId(Integer id);

    Assets create(Assets assets);

    Assets update(Integer id, Assets assets);

    void delete(Integer id);
}
