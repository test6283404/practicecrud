package com.martin.service;

import com.martin.model.bean.Assets;
import com.martin.model.dao.AssetsDao;
import com.martin.model.dao.AssetsDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * 處理 Assets 的邏輯判斷
 */
@Service
public class AssetsService {

    @Autowired
    private AssetsDaoImpl assetsDao;

    /**
     * 找出所有 Assets
     * @return List<Assets>
     */
    public List<Assets> findAll() {
        return assetsDao.getAll();
    }

    /**
     * 找特定的 Assets
     * @param id
     * @return Assets
     */
//    public Assets findById(Integer id) {
//        return assetsDao.findById(id).orElse(null);
//    }

    /**
     * 新增一筆資料
     * @param assets
     * @return Assets
     */
//    public Assets create(Assets assets) {
//        String builtDate = assets.getBuiltDate();
//
//        return checkTimeFormat(builtDate) ? assetsDao.save(assets) : null;
//    }

    /**
     * 更新一筆資料
     * @param id
     * @param assets
     * @return Assets
     */
//    public Assets update(Integer id, Assets assets) {
//        if (checkTimeFormat(assets.getBuiltDate())) {
//            return null;
//        }
//
//        Assets existingAssets = assetsDao.findById(id).orElse(null);
//
//        if (existingAssets != null) {
//            existingAssets.setBuiltDate(assets.getBuiltDate());
//            existingAssets.setName(assets.getName());
//            existingAssets.setUnit(assets.getUnit());
//            existingAssets.setUser(assets.getUser());
//            existingAssets.setValue(assets.getValue());
//
//            return assetsDao.save(existingAssets);
//        }
//        return null;
//    }

    /**
     * 刪除特定資料
     * @param id
     */
    public void delete(Integer id) {
        assetsDao.delete(id);
    }

    /**
     * 對傳入的時間進行正規判斷
     * @param date
     * @return boolean
     */
    private boolean checkTimeFormat(String date) {
        String regex = "^\\d{4}[\\-/\\.](0?[1-9]|1[012])[\\-/\\.](0?[1-9]|[12][0-9]|3[01])$";
        Pattern p = Pattern.compile(regex);

        return p.matcher(date).find();
    }
}
